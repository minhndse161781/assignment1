﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    public class ThucThi
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Nhap so luong Giao Vien: ");
                int soLuong = int.Parse(Console.ReadLine());
                List<GiaoVien> listGV = new List<GiaoVien>();

                for (int i = 0; i < soLuong; i++)
                {
                    GiaoVien giaoVien = new GiaoVien();
                    Console.WriteLine($"Nhap thong tin Giao vien thu {i + 1}");
                    giaoVien.NhapThongTin();
                    listGV.Add(giaoVien);
                }

                GiaoVien giaoVienThapNhat = listGV.OrderBy(x => x.TinhLuong()).FirstOrDefault();
                if (giaoVienThapNhat != null)
                {
                    Console.WriteLine("Giao vien luong thap nhat: ");
                    giaoVienThapNhat.XuatThongTin();
                }

                Console.Read();
            }
            catch (FormatException)
            {
                Console.WriteLine("Du lieu khong đung đinh dang.");
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("Co đoi tuong hoac bien null.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Chuong trinh bi loi o: " + e.Message);

            }
        }
    }

}
