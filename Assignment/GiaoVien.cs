﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    public class GiaoVien : NguoiLaoDong
    {
        private double HeSoLuong { get; set; }

        public GiaoVien() { }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base (hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }

        public void NhapThongTin()
        {
            base.NhapThongTin();
            Console.WriteLine("Nhap he so luong: ");
            HeSoLuong = Convert.ToDouble(Console.ReadLine());
        }

        public double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }

        public void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine("He So Luong: " + HeSoLuong + "," + "Luong: " + TinhLuong());
        }

        public void XuLy()
        {
            HeSoLuong += 0.6;
        }


    }
}
